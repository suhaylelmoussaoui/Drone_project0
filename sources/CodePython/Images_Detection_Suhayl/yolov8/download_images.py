#Exercice 1
a = {'F':['B','G'], 'B':['A','D'], 'A':['',''], 'D':['C','E'], \

'C':['',''], 'E':['',''], 'G':['','I'], 'I':['','H'], \

'H':['','']}

k=0

def taille(a,s):

    global k

    if s in a:

        k=k+1  

        if a[s][0]!='':        

            taille(a,a[s][0])

        if a[s][1]!='':   

            taille(a,a[s][1])              

        else: k=k

        return k  


taille(a, 'F')
#Exercice 2
def tri_iteratif(tab):
    for k in range(len(tab)-1, 0, -1):
        imax = 0
        for i in range(0, k+1):
            if tab[i] > tab[imax]:
                imax = i
        if tab[imax] > tab[k]:
            tab[k], tab[imax] = tab[imax], tab[k]
    return tab

# Test de la fonction
print(tri_iteratif([41, 55, 21, 18, 12, 6, 25]))
