"""Le code suivant sert dans le cas où un gripper est mise en place sur le drone, ce code pour un servo motor, permet de fermer et d'ouvrir cette pince."""
from gpiozero import AngularServo  # Importation de la classe AngularServo de la bibliothèque gpiozero
import time  # Importation de la bibliothèque time pour les délais

myGPIO = 18  # Définition du numéro du GPIO auquel le servo est connecté
SERVO_DELAY_SEC = 0.001  # Délai entre chaque ajustement d'angle pour le servo, en secondes
myCorrection = 0.0  # Correction à appliquer aux largeurs d'impulsion si nécessaire
maxPW = (2.5 + myCorrection) / 1000  # Calcul de la largeur d'impulsion maximale en secondes
minPW = (0.5 - myCorrection) / 1000  # Calcul de la largeur d'impulsion minimale en secondes

# Création de l'objet servo avec les paramètres définis
servo = AngularServo(myGPIO, initial_angle=0, min_angle=0,
                     max_angle=180, min_pulse_width=minPW, max_pulse_width=maxPW)

def loop():
    while True:  # Boucle infinie
        for angle in range(0, 181, 1):  # Fait tourner le servo de 0 à 180 degrés
            servo.angle = angle  # Définit l'angle du servo
            time.sleep(SERVO_DELAY_SEC)  # Attend un peu pour permettre au servo de bouger
        time.sleep(0.5)  # Attend un peu entre les changements de direction
        for angle in range(180, -1, -1):  # Fait tourner le servo de 180 à 0 degrés
            servo.angle = angle  # Définit l'angle du servo
            time.sleep(SERVO_DELAY_SEC)  # Attend un peu pour permettre au servo de bouger
        time.sleep(0.5)  # Attend un peu entre les changements de direction

if __name__ == '__main__':  # Entrée du programme
    print('Le programme démarre...')  # Affiche un message au démarrage
    try:
        loop()  # Exécute la boucle principale
    except KeyboardInterrupt:  # Appuyez sur ctrl-c pour terminer le programme.
        print("Fin du programme")  # Affiche un message avant de terminer
