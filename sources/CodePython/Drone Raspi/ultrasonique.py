"""Ce programme permet à un sensor d'ultrason, de détecter le sol ou des obstacles pour les éviter par la suite"""
import os
os.system("sudo pigpiod")  # Lance le daemon pigpiod pour permettre le contrôle des GPIO via pigpio

from gpiozero import DistanceSensor  # Importe la classe DistanceSensor de la bibliothèque gpiozero
from gpiozero.pins.pigpio import PiGPIOFactory  # Importe PiGPIOFactory pour utiliser pigpio comme backend GPIO
from time import sleep  # Importe sleep de la bibliothèque time pour les délais

trigPin = 23  # Définit le numéro de pin GPIO pour le signal de déclenchement du capteur ultrason
echoPin = 24  # Définit le numéro de pin GPIO pour le signal d'écho du capteur ultrason
my_factory = PiGPIOFactory()  # Crée une instance de PiGPIOFactory

# Crée une instance de DistanceSensor avec les pins spécifiées, une distance maximale de détection de 3 mètres, 
# et utilise my_factory pour le contrôle des pins
sensor = DistanceSensor(echo=echoPin, trigger=trigPin, max_distance=3, pin_factory=my_factory)

def loop():
    while True:  # Boucle infinie pour mesurer la distance continuellement
        print('Distance: ', sensor.distance * 100, 'cm')  # Affiche la distance mesurée en centimètres
        sleep(1)  # Attend une seconde entre chaque mesure

if __name__ == '__main__':  # Point d'entrée du programme
    print('Le programme démarre...')  # Affiche un message au démarrage
    try:
        loop()  # Exécute la fonction loop
    except KeyboardInterrupt:  # Gère l'interruption du programme avec Ctrl-C
        sensor.close()  # Nettoie et libère les ressources du capteur
        os.system("sudo killall pigpiod")  # Arrête le daemon pigpiod
        print("Fin du programme")  # Affiche un message indiquant la fin du programme
