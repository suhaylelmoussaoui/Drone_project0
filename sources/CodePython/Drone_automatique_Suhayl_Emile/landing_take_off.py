#############DÉPENDANCES#######################

from dronekit import connect, VehicleMode, LocationGlobalRelative, APIException
import time
import socket
import exceptions
import math

##############FONCTIONS##########################

##Fonction pour armer les hélices du drone et décoller à targetHeight (m)

def arm_and_takeoff(targetHeight):

    while vehicle.is_armable!=True:
        print("En attente que le véhicule devienne armable.")
        time.sleep(1)
    print("Le véhicule est maintenant armable")

    vehicle.mode = VehicleMode("GUIDED")

    while vehicle.mode!='GUIDED':
        print("En attente que le drone entre en mode de vol GUIDED")
        time.sleep(1)
    print("Véhicule en MODE GUIDED. Amusez-vous bien !!")

    vehicle.armed = True
    while vehicle.armed==False:
        print("En attente que le véhicule soit armé.")
        time.sleep(1)
    print("Attention ! Les hélices virtuelles tournent !!")

    vehicle.simple_takeoff(targetHeight)

    while True:
        print("Altitude actuelle : %d"%vehicle.location.global_relative_frame.alt)
        if vehicle.location.global_relative_frame.alt>=.95*targetHeight:
            break
        time.sleep(1)
    print("Altitude cible atteinte !!")

    return None


############EXÉCUTABLE PRINCIPAL#############

####sim_vehicle.py ouvre un port sur localhost:14550

vehicle = connect('127.0.0.1:14550', wait_ready=True)

####Armer le drone et décoller dans les airs à 5 mètres

arm_and_takeoff(5)
print("Le véhicule a atteint l'altitude cible")

####Une fois que le drone atteint l'altitude cible, changer le mode en LAND

vehicle.mode=VehicleMode('LAND')
while vehicle.mode!='LAND':
    print("En attente que le drone entre en mode LAND")
    time.sleep(1)
print("Véhicule en mode LAND. Touchera le sol sous peu.")
