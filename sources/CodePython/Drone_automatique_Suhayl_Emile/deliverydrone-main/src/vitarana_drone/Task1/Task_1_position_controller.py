#!/usr/bin/env python

# Importation des bibliothèques requises

from vitarana_drone.msg import *
from sensor_msgs.msg import NavSatFix
from std_msgs.msg import Float32
import rospy
import numpy as np

class Position():

    def __init__(self):
        # Initialisation du nœud ROS avec le nom node_position_controller
        rospy.init_node('node_position_controller')

        # Tableau Numpy pour les points de consigne des coordonnées GPS
        self.setpoints = np.array(
            [[19.0, 72.0, 3.0], [19.0000451704, 72.0, 3.0], [19.0000451704, 72.0, 0.31]])

        # Tableau Numpy pour la localisation GPS actuelle
        self.currentloc = np.array([0.0, 0.0, 0.0])
        # Index du point de consigne GPS vers lequel le drone se dirige
        self.loc = 0
        # Compteur pour la Stabilité
        self.stabilize = 0
        # Message Drone Command de type edrone_cmd et initialisation
        self.setpoint_rpy = edrone_cmd()
        self.setpoint_rpy.rcRoll = 0
        self.setpoint_rpy.rcPitch = 0
        self.setpoint_rpy.rcYaw = 0
        self.setpoint_rpy.rcThrottle = 0

        # Tableau Numpy pour les gains PID : [Latitude, Longitude, Altitude]
        self.Kp = np.array([720 * 6000, 475 * 6000, 225 * 0.6])
        self.Ki = np.array([55 * 0.08, 0 * 0.8, 4 * 0.008])
        self.Kd = np.array([1550 * 12000, 1450 * 12000, 465 * 0.3])

        # Pour stocker l'erreur précédente pour le terme dérivatif
        self.prev_values = np.array([0.0, 0.0, 0.0])
        # Pour stocker la somme des erreurs pour le terme intégral
        self.integral = np.array([0.0, 0.0, 0.0])
        # Valeurs maximales et minimales pour les commandes de roulis, de tangage et d'accélérateur
        self.max_value = 2000
        self.min_value = 1000

        # Taux d'échantillonnage et temps du PID
        self.sample_rate = 10  # en Hz
        self.sample_time = 0.1  # en secondes

        # Publication sur /edrone/drone_command, /altitude_error, /latitude_error, /longitude_error
        self.setpoint_pub = rospy.Publisher(
            '/edrone/drone_command', edrone_cmd, queue_size=1)

        # Souscription à /edrone/gps pour la localisation GPS actuelle
        rospy.Subscriber('/edrone/gps', NavSatFix, self.gps_callback)

    # Callback pour la localisation GPS
    def gps_callback(self, msg):
        self.currentloc = np.array([msg.latitude, msg.longitude, msg.altitude])

    # Fonction pour vérifier les limites de sortie du PID
    def checkLimits(self, drone_command):
        if drone_command > self.max_value:
            return self.max_value
        elif drone_command < self.min_value:
            return self.min_value
        else:
            return drone_command

    # Algorithme PID
    def pid(self):
        if not np.any(self.currentloc):
            return

        # Calcul de l'erreur et arrondissement à 7 décimales
        error = np.round((self.setpoints[self.loc] - self.currentloc), 7)

        # Calcul du terme dérivatif et arrondissement
        derivative = np.round(
            ((error - self.prev_values) / self.sample_time), 7)

        # Calcul du terme intégral et arrondissement
        self.integral = np.round(
            ((self.integral + error) * self.sample_time), 7)

        # Calcul de la sortie PID et arrondissement
        output = np.round(
            ((self.Kp * error) + (self.Ki * self.integral) + (self.Kd * derivative)), 7)

        # Calcul du gaz, du roulis et du tangage et vérification des limites
        throttle = self.checkLimits(1500.0 + output[2])
        pitch = self.checkLimits(1500.0 + output[1])
        roll = self.checkLimits(1500.0 + output[0])

        # Attribution des prev_values avec l'erreur pour la prochaine itération
        self.prev_values = error

        # Publication de la sortie PID finale sur /edrone/drone_command pour le contrôleur d'attitude
        self.setpoint_rpy.rcRoll = roll
        self.setpoint_rpy.rcPitch = pitch
        self.setpoint_rpy.rcYaw = 1500
        self.setpoint_rpy.rcThrottle = throttle
        self.setpoint_pub.publish(self.setpoint_rpy)

        # Vérification de la condition pour le seuil des points de consigne
        if abs(
            error[1]) < 0.0000047487 and abs(
            error[0]) < 0.000004517 and abs(
                error[2]) < 0.2:
            # Vérification si le drone a atteint le seuil du dernier point de consigne
            if self.loc == 2:
                self.setpoint_rpy.rcThrottle = 1000
                self.setpoint_rpy.rcRoll = 1500
                self.setpoint_rpy.rcPitch = 1500
                self.setpoint_rpy.rcYaw = 1500
                self.setpoint_pub.publish(self.setpoint_rpy)
                return
            # Comptage jusqu'à 10 avec la variable stabiliser pour que le drone se stabilise au point de consigne
            if self.stabilize < 15:
                self.stabilize += 1
                return
            # Incrémentation de l'index de localisation pour que le prochain point de consigne GPS soit défini
            self.loc += 1
            # Réinitialisation de stabiliser à 0 pour qu'il puisse être utilisé pour le prochain point de consigne
            self.stabilize = 0


if __name__ == '__main__':

    e_drone_position = Position()
    # Définition du taux rospy pour que l'algorithme PID tourne à la fréquence d'échantillonnage désirée
    r = rospy.Rate(e_drone_position.sample_rate)
    while not rospy.is_shutdown():
        # Appel de la fonction PID
        e_drone_position.pid()
        r.sleep()
