```python
#!/usr/bin/env python

# Importation des bibliothèques nécessaires

from vitarana_drone.msg import *
from sensor_msgs.msg import Imu
from std_msgs.msg import Float32
import rospy
import tf
import numpy as np

class Attitude():

    def __init__(self):
        # Initialisation du nœud ROS avec le nom attitude_controller
        rospy.init_node('node_attitude_controller')

        # Ceci correspond à l'orientation actuelle du eDrone au format quaternion
        # [x,y,z,w]
        self.drone_orientation_quaternion = np.array([0.0, 0.0, 0.0, 0.0])

        # Ceci correspond à l'orientation actuelle du eDrone convertie en angles d'Euler.
        # [p,r,y]
        self.drone_orientation_euler = np.array([0.0, 0.0, 0.0])

        # Souscrit à /edrone/drone_command
        # [p_setpoint, r_setpoint, y_setpoint]
        self.setpoint_cmd = np.array([0.0, 0.0, 0.0])
        self.throttle = 0.0

        # Le point de consigne de l'orientation en angles d'Euler
        # [p_setpoint, r_setpoint, y_setpoint]
        self.setpoint_euler = np.array([0.0, 0.0, 0.0])

        # Déclaration de pwm_cmd de type de message prop_speed et initialisation des valeurs
        self.pwm_cmd = prop_speed()
        self.pwm_cmd.prop1 = 0.0
        self.pwm_cmd.prop2 = 0.0
        self.pwm_cmd.prop3 = 0.0
        self.pwm_cmd.prop4 = 0.0

        # Tableau Numpy pour les gains PID : [Pitch, Roll, Yaw] * ratios de coefficients
        self.Kp = np.array([20, 20, 108]) * 0.06
        self.Ki = np.array([11, 11, 8]) * 0.008
        self.Kd = np.array([36, 36, 306]) * 0.03

        # Pour stocker l'erreur précédente pour le terme dérivé
        self.prev_values = np.array([0.0, 0.0, 0.0])
        # Pour stocker la somme des erreurs pour le terme intégral
        self.integral = np.array([0.0, 0.0, 0.0])
        # Valeurs maximales et minimales pour les commandes pwm
        self.max_value = 1023.0
        self.min_value = 0.0

        # Taux d'échantillonnage et temps du PID
        self.sample_rate = 10  # en Hz
        self.sample_time = 0.1  # en secondes

        # Publication sur /edrone/pwm, /roll_error, /pitch_error, /yaw_error
        self.pwm_pub = rospy.Publisher('/edrone/pwm', prop_speed, queue_size=1)

        # Souscription à /drone_command, imu/data, /pid_tuning_roll, /pid_tuning_pitch, /pid_tuning_yaw
        rospy.Subscriber('/edrone/drone_command', edrone_cmd, self.drone_command_callback)
        rospy.Subscriber('/edrone/imu/data', Imu, self.imu_callback)

    # Callback pour l'orientation provenant de l'IMU au format quaternion
    def imu_callback(self, msg):
        self.drone_orientation_quaternion = [msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w]

    # Callback pour les consignes de roulis, tangage, lacet et accélérateur
    def drone_command_callback(self, msg):
        self.setpoint_cmd = np.array([msg.rcPitch, msg.rcRoll, msg.rcYaw])
        self.throttle = msg.rcThrottle

    # Fonction pour vérifier les limites de sortie du PID
    def checkLimits(self, pwm):
        if pwm > self.max_value:
            return self.max_value
        elif pwm < self.min_value:
            return self.min_value
        else:
            return pwm

    # Algorithme PID
    def pid(self):
        # Conversion du quaternion en angles d'Euler
        self.drone_orientation_euler = np.array(tf.transformations.euler_from_quaternion(self.drone_orientation_quaternion))

        # Initialement, le point de consigne n'est pas mis à jour, c'est-à-dire [0,0,0]
        if not np.any(self.set